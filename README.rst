This repository contains the GUI packages for the PickOne Vision System.

.. contents:: Table of Contents
   :depth: 3

Instructions for Simulating PickOne
===================================

Utilizing the power of `Docker Compose <https://docs.docker.com/compose/overview/>`__, it is now only a simple two step process. What is Docker Compose? It is an orchestration tool for Docker containers. A single yaml file describes all the necessary steps to build, deploy, and glue any necessary parts together. Take a look at the docker-compose.yml file; if you are familiar with a Dockerfile much of it should seem self-explanatory, but please take a look at the official documentation `here <https://docs.docker.com/compose/compose-file/>`__.

Install Docker Compose
----------------------

If you have not already, install Docker Compose on your local machine. The steps to do so are `here <https://docs.docker.com/compose/install/>`__.

::

    sudo curl -L https://github.com/docker/compose/releases/download/1.20.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

    sudo chmod +x /usr/local/bin/docker-compose

That is it!

Generate Access Tokens && Log into Gitlab
-----------------------------------------

Thankfully you will only need to do this step once. First generate an access token. This is both used to login to gain access to the registry, but also to allow the containers to pull the source code from the repository without the need of passing personal keys around.

Instructions to generate Gitlab token `here <https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html>`__, but if you do not will to read all of that: simple go to your `Gitlab profile settings then click Access Tokens <https://gitlab.com/profile/personal_access_tokens>`__.

Add a personal access token on the right, you can leave ``Expires at`` blank to have it never expire, and click all the checkboxes under scopes. Finally click ``Create personal access token`` and a key will be shown above. **Be sure to copy this down**, as it will be the only time you can do so.

Now you want to copy the ``.secret-example`` file in each ``/container`` directory with this token.

::

    cp ./web_gui/container/.secret-example ./web_gui/container/.secret
    cp ./sensor_sim/container/.secret-example ./sensor_sim/container/.secret

Open up those ``.secret`` files and replace ``<generate your gitlab access token>`` with your personal access key.

Now login:

::

    docker login registry.gitlab.com -u <gitlab user> -p <personal access token>

Place bag files into /bags
--------------------------

Place any ``.bag`` files into the ``<bolles_gui root>/bags`` folder in the root repository. Currently it is hardcoded to run the ``pickone_sensor_data.bag`` file, but this will be changed soon.

The ``pickone_sensor_data.bag`` file can be found here: `pickone_sensor_data.bag <https://plusonerobotics.sharepoint.com/:u:/s/SoftwareBagStorage/EWE94lB8ompOvkuzohgS-ooBGSgNpBTVAAioh2s1RAgGiQ?e=ZFZHri/>`_ (997MB). This link requires a PlusOne Robotics email account.

Run Docker Compose
------------------

Now in the ``bolles_gui`` base directory with the ``docker-compose.yml`` file, simply run:

::

    docker-compose up --build

Give it a minute or two as it pulls the images, compiles the source, and runs the appropriate binaries. You should see some output describing as such, though eventually it will launch the simulator along with the reactjs web gui viewer.

If you get any errors, please check to make sure you have your personal access key in the ``.secret`` file otherwise hit me (@shazzner) up on Slack!

View the Web Gui
----------------

Open up a browser to:

::

    http://localhost:3000/

To view the results! Oh did you think there were more steps? Relax. :)

To stop the simulator and web gui, simply press ``Ctrl-c`` on your keyboard to safely stop.

(Optional) Run Docker Compose in detached mode
----------------------------------------------

Optionally, if you want to free your terminal up. You can run Docker Compose in detached mode:

::

    docker-compose up -d --build

This will do all building and deploying in the background. To stop the process simply run:

::

    docker-compose down

You can find more information on various Docker Compose commands `here <https://docs.docker.com/compose/reference/overview/>`__.

Pick Position Request
---------------------

Local Terminal
~~~~~~~~~~~~~~

::

    docker ps (to get container name)
    docker exec -it <Container Name> bash

Inside Docker
~~~~~~~~~~~~~

::

    source install/setup.bash

    rosservice call /trigger/get_pick_position "{}"
    rosservice call /trigger_place/get_pick_position "{}"

Web Components
==============

Rosbridge Suite
---------------

Tutorials can be found `here <http://wiki.ros.org/rosbridge_suite/Tutorials>`__.

Web Video Server
----------------

Tutorials can be found `here <http://wiki.ros.org/web_video_server>`__.

To modify the port for the web\_video\_server, pass it in from the command line:

::

    roslaunch bolles_sensor_simulator upstart.launch bag_file:=<Path to bag> port:=<PORT>

    Example:
    roslaunch bolles_sensor_simulator upstart.launch bag_file:=/root/catkin_ws/bags/pick_one_sensor_data.bag port:=8080

Note: The default port is set to 9091.

Example to Access from Browser:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    http://localhost:9091/stream?topic=/kinect/rgb/image_rect_color
